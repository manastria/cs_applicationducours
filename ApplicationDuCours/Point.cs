﻿namespace ApplicationDuCours
{
    public class Point
    {
        public int x;
        public int y;

        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public string GetInfo() {
            string resultat="";

            resultat = resultat + "Point: x: " + x + ", y: " + y ;

            return resultat;
        }

        public void Deplacer(int x, int y)
        {
            this.x += x;
            this.y += y;
        }
    }
}
