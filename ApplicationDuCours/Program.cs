﻿using System;

namespace ApplicationDuCours
{
    class Program
    {
        static void Main(string[] args)
        {
            Point p = new Point(5, 10);

            Console.WriteLine(p.GetInfo());

            Console.WriteLine("Appuyer sur une touche pour quitter");
            Console.ReadKey();
        }
    }
}
